public class SwitchStatement {
	public static void main(String args[]) {
		char choice;
		System.out.println(" Select your choice between 1 to 5: ");
		try {
			switch (choice = (char) System.in.read()) {
			case '1':

				System.out.println(" Pune ");
				break;

			case '2':
				System.out.println(" Bombay ");
				break;

			case '3':
				System.out.println(" Chennai ");
				break;

			case '4':
				System.out.println(" Noida ");
				break;

			case '5':
				System.out.println(" Bangalore ");
				break;

			default:
				System.out.println(" Invalid choice ");
			}
		} catch (Exception e) {
			System.out.println(" Error ");
		}
	}
}
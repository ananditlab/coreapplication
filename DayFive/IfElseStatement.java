public class IfElseStatement {
	public static void main(String args[]) {
		int a = 5, b = 6, c = 8;
		if (c >= b && c>=a) {
			System.out.println(c + " is the largest..");
		}
		else if(b>=a && c>=b){
			System.out.println(b+" is the largest number..");
		}
	}
}

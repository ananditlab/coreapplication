public class Chain {

	Chain() {

		System.out.println("Constructor number 1 is called!!");

	}
}

public class Chain2 {
	Chain2() {

		System.out.println("constructor 2 is called!!");
	}
}

public class Chain4 {

	String name;

	Chain4(String name) {
		this.name = name;
	}

}

class CallCons {

	public static void main(String args[]) {
		Chain C = new Chain();
		Chain2 C2 = new Chain2();
		Chain4 C4 = new Chain4("OpenIT!!");
		System.out.println("name:" + C4.name);
	}
}